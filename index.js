const { Master } = require('@commonshost/server')
const { connect } = require('http2')
const { receiveHttp2: fetch } = require('./receiveHttp2')
const { performance } = require('perf_hooks')
const { join } = require('path')
const CuckooFilter = require('cuckoofilter-native')

async function main () {
  const root = join(__dirname,
    'node_modules/@fortawesome/fontawesome-free/svgs')
  const options = {
    workers: { count: 1 },
    log: { level: 'silent' },
    push: {
      // TODO: cuckoofilter-native parameter for bits per item
      diaryBitsPerItem: 12,
      // Tweak this to make the Cuckoo filter larger or smaller
      diaryTotalItems: 2048
    },
    hosts: [{
      root,
      // Each filepath pushes different subresources.
      // Server tracks what was pushed in the Cuckoo filter.
      // If the filter is too small, false negatives occur and
      // subresources are pushed again (over-push).
      manifest: [
        {
          get: '/brands/firefox.svg',
          push: '/regular/*.svg'
        },
        {
          get: '/brands/chrome.svg',
          push: '/solid/*.svg'
        },
        {
          get: '/brands/500px.svg',
          push: '/brands/*.svg'
        }
      ]
    }]
  }
  const server = new Master({ options })
  await server.listen()

  {
    const totalItems = options.push.diaryTotalItems
    const { bytes } = new CuckooFilter(totalItems)
    console.log(`Cuckoo filter: ${totalItems.toLocaleString()} slots` +
      ` @ ${bytes.toLocaleString()} bytes\n`)
  }

  const { origin } = new URL('https://localhost:8443')

  // Do requests a few times to show the difference
  const repeat = 3

  // URL pathnames to trigger push
  const paths = [
    '/brands/firefox.svg',
    '/brands/chrome.svg',
    '/brands/500px.svg'
  ]

  console.log(`Connecting to ${origin}\n`)
  const session = connect(origin, {
    maxReservedRemoteStreams: 2 ** 32 - 1,
    rejectUnauthorized: false
  })

  for (const path of paths) {
    const url = origin + path
    const header = `GET ${path}`
    console.log(`${header}\n${header.replace(/./g, '-')}`)
    for (let iteration = 1; iteration <= repeat; iteration++) {
      console.log(`- Request ${iteration}`)
      const before = performance.now()
      const response = await fetch(url, { session })
      const after = performance.now()
      const bytes = response.push
        .map(({ data }) => data.length)
        .reduce((accumulator, length) => accumulator += length, 0)
        .toLocaleString()
      console.log(`  Duration:     ${Math.round(after - before)} ms`)
      console.log(`  Pushed files: ${response.push.length}`)
      console.log(`  Pushed size:  ${bytes} bytes`)
    }
    console.log()
  }

  console.log('Disconnecting')
  await new Promise(session.close.bind(session))
  await server.close()
}

main().catch(console.error)
