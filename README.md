# demo-cache-digest-push-diary

Demo to research efficacy of an HTTP Server Push Cache Digest using Cuckoo Filters.

While Cache Digests would be implemented in the client (browser), the simpler Push Diary implementation can be done purely server-side.

The demo uses the popular Font Awesome SVG icons library. These are ~1500 icons typically distributed in concatenated bundles in various formats: JS/CSS/SVG/fonts.

When a single icon is added/removed/edited, the entire bundle is invalidated and `n-1/n` of the icons are re-downloaded needlessly.

The code (see: [`/index.js`](./index.js)) runs an HTTP/2 client and server in Node.js. The client receives the icon SVG files as PUSH_PROMISE/HEADER/DATA frames. Using a sufficiently sized "push diary", held in memory by the server, subsequent requests on the same client connection omit all subresources which are presumed to still be cached.

## Output

A single connection is established by the client, so the server re-uses its push diary. Each request is repeated 3 times to show that only the first time the subresources are pushed.

```
$ npm run start

> @ start /Users/sebdeckers/code/commonshost/test-push-cache-digest
> node index.js | npx pino-colada

Cuckoo filter: 2,048 slots @ 6,144 bytes

Connecting to https://localhost:8443

GET /brands/firefox.svg
-----------------------
- Request 1
  Duration:     113 ms
  Pushed files: 148
  Pushed size:  104,654 bytes
- Request 2
  Duration:     11 ms
  Pushed files: 0
  Pushed size:  0 bytes
- Request 3
  Duration:     9 ms
  Pushed files: 0
  Pushed size:  0 bytes

GET /brands/chrome.svg
----------------------
- Request 1
  Duration:     274 ms
  Pushed files: 798
  Pushed size:  517,659 bytes
- Request 2
  Duration:     27 ms
  Pushed files: 0
  Pushed size:  0 bytes
- Request 3
  Duration:     19 ms
  Pushed files: 0
  Pushed size:  0 bytes

GET /brands/500px.svg
---------------------
- Request 1
  Duration:     101 ms
  Pushed files: 307
  Pushed size:  310,661 bytes
- Request 2
  Duration:     16 ms
  Pushed files: 0
  Pushed size:  0 bytes
- Request 3
  Duration:     18 ms
  Pushed files: 0
  Pushed size:  0 bytes

Disconnecting
```
