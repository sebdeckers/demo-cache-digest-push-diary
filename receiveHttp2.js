const { connect } = require('http2')
const { URL } = require('url')
const { StringDecoder } = require('string_decoder')

class Response {
  constructor (headers, body, push) {
    this.headers = new Map(Object.entries(headers))
    this.body = body
    this.push = push
  }

  async text () {
    return this.body.length === 0
      ? ''
      : new StringDecoder().end(this.body)
  }

  async json () {
    return JSON.parse(await this.text())
  }

  async arrayBuffer () {
    return this.body
  }

  get status () {
    return this.headers.get(':status')
  }

  get ok () {
    const { status } = this
    return status >= 200 && status <= 400
  }
}

function receiveHttp2 (url, options = {}) {
  return new Promise((resolve, reject) => {
    const pending = new Set()
    let result
    const push = []
    const done = (stream, payload) => {
      if (pending.has(stream)) {
        pending.delete(stream)
        if (payload) {
          push.push(payload)
        }
        if (pending.size === 0) {
          if (options.session) onDone()
          else session.close(onDone)
        }
      }
    }
    const onDone = () => {
      session.removeAllListeners('stream')
      resolve(new Response(result.response, result.data, push))
    }

    const session = options.session || connect(url, Object.assign({
      maxReservedRemoteStreams: 2 ** 32 - 1,
      rejectUnauthorized: false,
      lookup: (hostname, options, callback) => {
        callback(null, '127.0.0.1', 4)
      }
    }, options))
    const { pathname, search } = new URL(url)
    const headers = Object.assign(
      {
        ':path': pathname + search,
        ':method': options.method || 'GET'
      },
      options.headers
    )
    const request = session.request(headers)
    session.on('stream', (stream, request, flags) => {
      pending.add(stream)
      const chunks = []
      const received = { request }
      stream.on('data', (chunk) => chunks.push(chunk))
      stream.on('push', (response, flags) => {
        received.response = response
        received.flags = flags
      })
      stream.on('close', () => {
        if (chunks.length > 0) {
          received.data = Buffer.concat(chunks)
        }
        received.rstCode = stream.rstCode
        done(stream, received)
      })
      stream.on('error', (error) => {
        done(stream, { request, error })
      })
    })
    request.on('response', (response, flags) => {
      pending.add(response)
      const chunks = []
      request.on('data', (chunk) => chunks.push(chunk))
      request.on('close', () => {
        const data = Buffer.concat(chunks)
        result = { response, data }
        done(response)
      })
    })
    request.end(options.body)
  })
}

module.exports.receiveHttp2 = receiveHttp2
